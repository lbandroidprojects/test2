package com.example.lukaszb.test2;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity
        implements View.OnClickListener, RadioGroup.OnCheckedChangeListener, Spinner.OnItemSelectedListener{
    private Button b1, b2;
    private EditText et;
    private TextView tv,tv2;
    private boolean wykonuj=true;
    private RadioGroup radioGroup;
    private ArrayList<String> kolory;
    private Spinner spinner;
    private ArrayAdapter<String> adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        b1=findViewById(R.id.b1);
        b1.setOnClickListener(this);
        b2=findViewById(R.id.b2);
        b2.setOnClickListener(this);
        et=findViewById(R.id.et);
        tv=findViewById(R.id.tv);
        radioGroup=findViewById(R.id.radioGroup);
        radioGroup.setOnCheckedChangeListener(this);
        radioGroup.check(R.id.rbWykonuj);

        spinner=findViewById(R.id.spinner);
        tv2=findViewById(R.id.tv2);
        kolory=new ArrayList<>();
        kolory.add("Czerwony");
        kolory.add("Zielony");
        kolory.add("Biały");
        kolory.add("Czarny");
        adapter=new ArrayAdapter<String>(this,android.R.layout.simple_spinner_dropdown_item,kolory);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(this);

    }

    @Override
    public void onClick(View view) {
        if (wykonuj) {
            switch (view.getId()) {
                case R.id.b1:
                    String text = et.getText().toString();
                    tv.setText(text);
                    break;
                case R.id.b2:
                    et.setText("");
                    tv.setText("");
                    break;
            }
        }
    }

    @Override
    public void onCheckedChanged(RadioGroup radioGroup, int i) {
       switch(i){
           case R.id.rbWykonuj:
               wykonuj=true;
               break;
           case R.id.rbIgnoruj:
               wykonuj=false;
               break;
       }
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        tv2.setText(((TextView)view).getText());
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }
}
